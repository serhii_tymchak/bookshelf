<?php

class authorModel {

  public $dbConnect;

  public function __construct() {

    include_once 'config/db.php';
    $db = new db ();

    if(! $db){
      echo "Ошибка подключения к MySql";
      exit();
    }
    $this->dbConnect = $db ->mysqli;
  }

  function getBookByAuthor() {

    $sql = "SELECT * FROM `books` WHERE `auth_id` = {$_GET['auth_id']}";

    $rs = mysqli_query($this->dbConnect, $sql);

    $booksArr = [];
    while ($row = mysqli_fetch_assoc($rs)) {
      $booksArr[] = $row;
    }

    return $booksArr;
  }

  function getAllAuthors() {

    $sql = "SELECT * FROM `authors`";

    $rs = mysqli_query($this->dbConnect, $sql);

    $authorsArr = [];
    while ($row = mysqli_fetch_assoc($rs)) {
      $authorsArr[] = $row;

    }

    return $authorsArr;
  }

}
