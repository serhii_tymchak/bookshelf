<?php

class indexModel {

  public $dbConnect;

  public function __construct() {

    include_once 'config/db.php';
    $db = new db ();

    if(! $db){
      echo "Ошибка подключения к MySql";
      exit();
    }
    $this->dbConnect = $db ->mysqli;
  }

    function getAllGenre (){

      $sql = "SELECT * FROM `genre`";

      $rs = mysqli_query($this->dbConnect, $sql);

      $genreArr = [];
      while ($row = mysqli_fetch_assoc($rs)){
        $genreArr[]= $row;
      }

      return $genreArr;
    }


    function getAllBooks (){

      $sql = "SELECT * FROM `books`";

      $rs = mysqli_query($this->dbConnect, $sql);

      $booksArr = [];
      while ($row = mysqli_fetch_assoc($rs)){
        $booksArr[]= $row;
      }

      return $booksArr;
    }

    function getAllAuthors (){

      $sql = "SELECT * FROM `authors`";

      $rs = mysqli_query($this->dbConnect, $sql);

      $authorsArr = [];
      while ($row = mysqli_fetch_assoc($rs)){
        $authorsArr[]= $row;

      }
      return $authorsArr;
    }

}




