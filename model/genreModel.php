<?php

class genreModel{

  public $dbConnect;

  public function __construct() {

    include_once 'config/db.php';
    $db = new db ();

    if(! $db){
      echo "Ошибка подключения к MySql";
      exit();
    }
    $this->dbConnect = $db ->mysqli;
  }



      function getBookByGenre (){

        $sql = "SELECT * FROM `books` WHERE `genre_id` = {$_GET['genre_id']}";

        $rs = mysqli_query($this->dbConnect, $sql);

        $booksArr = [];
        while ($row = mysqli_fetch_assoc($rs)){
          $booksArr[]= $row;
        }

        return $booksArr;
      }

      function getAllGenre (){

        $sql = "SELECT * FROM `genre`";

        $rs = mysqli_query($this->dbConnect, $sql);

        $genreArr = [];
        while ($row = mysqli_fetch_assoc($rs)){
          $genreArr[]= $row;

        }

        return $genreArr;
      }

}

