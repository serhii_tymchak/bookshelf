<?php include_once 'model/genreModel.php';
$genre = new genreModel();
$resGenre = $genre->getAllGenre();
$resBooksByGenre = $genre->getBookByGenre();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Book`s genre</title>

    <link href="/style/css/bootstrap.min.css" rel="stylesheet">
    <link href="/style/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

    <script src="/style/js/jquery/jquery-3.3.1.min.js"></script>

    <script src="/style/js/bootstrap.min.js"></script>

<div class="header">
  <h1><a href="/">Main page</a></h1>
</div>

<!-- блок с информацией о жанре-->
<div class="leftColumn">
    <div class="leftMenu">
        <div class="menuCaptain">Genre:</div>
      <?php foreach ($resGenre as $a): ?>
          <h3><a href="booksByGenre.php/?genre_id=<?php echo $a['genre_id']?>"> <?php echo $a['name']?></a></h3>
      <?php endforeach ?>
    </div>

<!-- блок с информацией о книгах-->
<div class="centerColumn">
  <?php foreach ($resBooksByGenre as $c): ?>
    <h3><a href="/bookPage.php/?book_id=<?php echo $c['book_id']?>"><?php echo $c['name']?></a></h3>
  <?php endforeach ?>
</div>



<div class="footer">
  <p>Footer</p>
</div>

</body>
