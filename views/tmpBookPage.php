<?php include_once 'model/bookModel.php';
$book = new bookModel();
$resBook = $book->getBook();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Book`s page</title>

    <link href="/style/css/bootstrap.min.css" rel="stylesheet">
    <link href="/style/css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

    <script src="/style/js/jquery/jquery-3.3.1.min.js"></script>

    <script src="/style/js/bootstrap.min.js"></script>

<div class="header">
  <h1><a href="/">Main page</a></h1>
</div>

<!--  блок с информацией о книге-->
<div>
  <?php foreach ($resBook as $c): ?>
    <h3><?php echo $c['name']?></h3>
  <?php endforeach ?>
</div>
<br>

    <form action="../mainfunction/formHandle.php" method="post" name="frm_feedback">
        <label>Ваше имя:</label><br/>
        <input type="text" name="user_name" value="<?=($_POST['user_name']) ? $_POST['user_name'] : ""; // сохраняем то, что вводили?>" /><br/>

        <label>Ваш e-mail:</label><br/>
        <input type="text" name="user_email" value="<?=($_POST['user_email']) ? $_POST['user_email'] : ""; // сохраняем то, что вводили?>" /><br/>

        <label>Текст сообщения:</label><br/>
        <textarea name="text_comment"><?=($_POST['text_comment']) ? $_POST['text_comment'] : ""; // сохраняем то, что вводили?></textarea>
        <br/>
        <input type="submit" value="Отправить" name="btn_submit" />
    </form>

<div class="footer">
  <p>Footer</p>
</div>


</body>
</html>





