<?php include_once 'model/indexModel.php';
$indexPage = new indexModel();
$resAllGenre = $indexPage ->getAllGenre();
$resAllAuthors = $indexPage ->getAllAuthors();
$resAllBooks = $indexPage ->getAllBooks();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Main page</title>

    <link href="/style/css/bootstrap.min.css" rel="stylesheet">
    <link href="/style/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>

    <script src="/style/js/jquery/jquery-3.3.1.min.js"></script>

    <script src="/style/js/bootstrap.min.js"></script>

<div class="header">
  <h1><a href="/">Main page</a></h1>
</div>


<!-- левый столбец-->
<div class="leftColumn">
  <div class="leftMenu">
    <div class="menuCaptain">Genre:</div>
    <?php foreach ($resAllGenre as $a): ?>
      <h3><a href="booksByGenre.php/?genre_id=<?php echo $a['genre_id']?>"> <?php echo $a['name']?></a></h3>
    <?php endforeach ?>
  </div>

  <div class="menuCaptain">Authors:</div>
  <?php foreach ($resAllAuthors as $b): ?>
    <h3><a href="booksByAuthor.php/?auth_id=<?php echo $b['auth_id']?>"> <?php echo $b['name']?></a></h3>
  <?php endforeach ?>
</div>
</div>

<!-- центральный столбец-->
<div class="centerColumn">
  <?php foreach ($resAllBooks as $c): ?>
    <h3><a href="bookPage.php/?book_id=<?php echo $c['book_id']?>"> <?php echo $c['name']?></a></h3>
  <?php endforeach ?>
</div> <!-- закрывающий тег центральной колонки -->

<div class="footer">
  <p>Footer</p>
</div>

</body>







